package br.com.fiap.jogodavelha.client.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import br.com.fiap.jogodavelha.modelo.Jogada;
import br.com.fiap.jogodavelha.modelo.Jogador;
import br.com.fiap.jogodavelha.suporte.Conexao;

public class Tabuleiro {

	private JFrame frame;
	private Conexao conexao;
	private Jogador jogador;

	private JLabel lblStatus;
	private JLabel lblNome;
	private JLabel lblMarca;

	private JButton btn0;
	private JButton btn1;
	private JButton btn2;
	private JButton btn3;
	private JButton btn4;
	private JButton btn5;
	private JButton btn6;
	private JButton btn7;
	private JButton btn8;
	
	private JButton btnNovo;
	
	private br.com.fiap.jogodavelha.modelo.Tabuleiro tabuleiro;


	/**
	 * Create the application.
	 */
	public Tabuleiro(Conexao conexao, Jogador jogador) {
		this.conexao = conexao;
		this.jogador = jogador;
		this.tabuleiro = new br.com.fiap.jogodavelha.modelo.Tabuleiro();
		initialize();
		
		Receiver r = new Receiver(conexao, jogador, tabuleiro, lblStatus, btn0, btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8);
		Thread t = new Thread(r);
		t.start();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		setFrame(new JFrame());
		getFrame().setResizable(false);
		getFrame().getContentPane().setBackground(Color.GRAY);
		getFrame().setBounds(100, 100, 326, 420);
		getFrame().setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getFrame().getContentPane().setLayout(null);
		
		this.btn0 = new JButton("");
		btn0.setEnabled(false);
		btn0.setFont(new Font("Tahoma", Font.PLAIN, 90));
		btn0.setBounds(0, 70, 100, 100);
		getFrame().getContentPane().add(btn0);
		btn0.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				joga(0);
			}
		});

		this.btn1 = new JButton("");
		btn1.setEnabled(false);
		btn1.setFont(new Font("Tahoma", Font.PLAIN, 90));
		btn1.setBounds(110, 70, 100, 100);
		getFrame().getContentPane().add(btn1);
		btn1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				joga(1);
			}
		});
		
		this.btn2 = new JButton("");
		btn2.setEnabled(false);
		btn2.setFont(new Font("Tahoma", Font.PLAIN, 90));
		btn2.setBounds(220, 70, 100, 100);
		getFrame().getContentPane().add(btn2);
		btn2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				joga(2);
			}
		});
		
		this.btn3 = new JButton("");
		btn3.setEnabled(false);
		btn3.setFont(new Font("Tahoma", Font.PLAIN, 90));
		btn3.setBounds(0, 181, 100, 100);
		getFrame().getContentPane().add(btn3);
		btn3.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				joga(3);
			}
		});
		
		this.btn4 = new JButton("");
		btn4.setEnabled(false);
		btn4.setFont(new Font("Tahoma", Font.PLAIN, 90));
		btn4.setBounds(110, 181, 100, 100);
		getFrame().getContentPane().add(btn4);
		btn4.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				joga(4);
			}
		});
		
		this.btn5 = new JButton("");
		btn5.setEnabled(false);
		btn5.setFont(new Font("Tahoma", Font.PLAIN, 90));
		btn5.setBounds(220, 181, 100, 100);
		getFrame().getContentPane().add(btn5);
		btn5.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				joga(5);
			}
		});
		
		this.btn6 = new JButton("");
		btn6.setEnabled(false);
		btn6.setFont(new Font("Tahoma", Font.PLAIN, 90));
		btn6.setBounds(0, 292, 100, 100);
		getFrame().getContentPane().add(btn6);
		btn6.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				joga(6);
			}
		});
		
		this.btn7 = new JButton("");
		btn7.setEnabled(false);
		btn7.setFont(new Font("Tahoma", Font.PLAIN, 90));
		btn7.setBounds(110, 292, 100, 100);
		getFrame().getContentPane().add(btn7);
		btn7.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				joga(7);
			}
		});
		
		this.btn8 = new JButton("");
		btn8.setEnabled(false);
		btn8.setFont(new Font("Tahoma", Font.PLAIN, 90));
		btn8.setBounds(220, 292, 100, 100);
		getFrame().getContentPane().add(btn8);
		btn8.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				joga(8);
			}
		});
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 320, 71);
		panel.setLayout(null);
		getFrame().getContentPane().add(panel);
		
		this.lblNome = new JLabel("Jogador: " + jogador.getNome());
		lblNome.setBounds(10, 10, 320, 15);
		panel.add(lblNome);
		
		this.lblMarca = new JLabel("Marca: " + jogador.getTipo().name());
		lblMarca.setBounds(10, 30, 320, 15);
		panel.add(lblMarca);
		
		this.lblStatus = new JLabel("");
		lblStatus.setBounds(10, 50, 320, 15);
		panel.add(lblStatus);
		
		this.btnNovo = new JButton("Novo");
		btnNovo.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnNovo.setBounds(240, 10, 70, 20);
		panel.add(btnNovo);
		btnNovo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				conexao.solicitaNovoJogo();
			}
		});
		
		
//		connect();
	}
	
	public void joga(Integer posicao) {
		Jogada jogada = new Jogada(posicao, jogador);
		conexao.enviaJogada(jogada);
	}
	

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}
}
