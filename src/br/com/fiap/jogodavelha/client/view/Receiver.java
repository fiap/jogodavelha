package br.com.fiap.jogodavelha.client.view;

import javax.swing.JButton;
import javax.swing.JLabel;

import br.com.fiap.jogodavelha.definicoes.Acoes;
import br.com.fiap.jogodavelha.modelo.Jogador;
import br.com.fiap.jogodavelha.modelo.Tabuleiro;
import br.com.fiap.jogodavelha.modelo.Quadrante;
import br.com.fiap.jogodavelha.modelo.TipoJogador;
import br.com.fiap.jogodavelha.suporte.Conexao;
import br.com.fiap.jogodavelha.suporte.XMLUtils;

public class Receiver implements Runnable{
	private final Conexao conexao;
	private final Jogador jogador;
	private final JLabel lblStatus;
	private JButton[] btns = new JButton[9];
	private Tabuleiro tabuleiro;


	public Receiver(Conexao conexao, Jogador jogador,Tabuleiro tabuleiro, JLabel lblStatus, JButton... btnQuadrantes) {
		this.conexao = conexao;
		this.jogador = jogador;
		this.tabuleiro = tabuleiro;
		this.lblStatus = lblStatus;
		this.btns    = btnQuadrantes;
	}
	
	
	public void processInput(String mensagemRecebida){
		String[] partesDaMensagem = mensagemRecebida.split(":", 2);
		System.out.println("proccessinput: " + mensagemRecebida);
		switch (Acoes.valueOf(partesDaMensagem[0])) {
			case ENVIA_TABULEIRO:
				this.tabuleiro = XMLUtils.tabuleiroFromXml(partesDaMensagem[1]);
				
				if (tabuleiro.temVencedor()){
					for (Quadrante quadrante : tabuleiro.getQuadrantes()) {
						JButton button = btns[quadrante.getCodigo()-1];
						if(!"-".equals(quadrante.getValor()) )
							button.setText(quadrante.getValor());
						
						button.setEnabled(false);
					}
					lblStatus.setText(tabuleiro.getJogadorUltimaJogada().getNome() + " ganhou");
					break;
				} else {
					if (!tabuleiro.hasNextMove()) {
						lblStatus.setText("Empatou");
						for (Quadrante quadrante : tabuleiro.getQuadrantes()) {
							JButton button = btns[quadrante.getCodigo()-1];
							button.setText(quadrante.getValor());
							button.setEnabled(false);
						}
						break;
					} else{
						for (Quadrante quadrante : tabuleiro.getQuadrantes()) {
							JButton button = btns[quadrante.getCodigo()-1];
							if(!"-".equals(quadrante.getValor())){
								button.setText(quadrante.getValor());
								button.setEnabled(false);
							} else {
								button.setText("");
								button.setEnabled(true);
							}
								
						}

						if (tabuleiro.getJogadorUltimaJogada().getNome().equals(jogador.getNome())){
							for (JButton btn: btns) {
								btn.setEnabled(false);
							}
						}
						break;
					}
				}
			case INICIIIA_PARTIDA:
				System.out.println("view iniciando a partida");
				if (jogador.getTipo() == TipoJogador.X){
//					for (JButton btn: btns) {
					for (int i = 0; i < btns.length; i++) {
						btns[i].setEnabled(true);
					}
				}
			break;
//			case SOMETHING:
//			break;
			default:
			break;
		}
	}
	
	@Override
	public void run() {
		while(this.conexao.isRegistered())
			this.processInput(this.conexao.receive());
//			this.read(false);
		
	}

}
