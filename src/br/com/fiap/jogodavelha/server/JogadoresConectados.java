package br.com.fiap.jogodavelha.server;

import java.util.HashMap;
import java.util.Map;

public class JogadoresConectados {
	private Map<String, JogadorInstance> jogadores = new HashMap<String, JogadorInstance>();
	private static JogadoresConectados jogadoresConectados = new JogadoresConectados();
	
	private JogadoresConectados(){
		setJogadorO(null);
		setJogadorX(null);
	}
	
	public static JogadoresConectados getInstance() {
		return jogadoresConectados;
	}
	
	public JogadorInstance getJogadorX(){
		return getJogador("X");
	}
	
	public JogadorInstance getJogadorO(){
		return getJogador("O");
	}
	
	public void setJogadorX(JogadorInstance jogadorX){
		jogadores.put("X", jogadorX);
	}
	
	public void setJogadorO(JogadorInstance jogadorO){
		setJogador("O", jogadorO);
	}
	
	public boolean temVaga(){
		return (getJogadorX() == null || getJogadorO() == null);
	}
	
	public int size(){
		int numeroJogadores = 0;
		if (getJogadorX() != null) numeroJogadores++;
		if (getJogadorO() != null) numeroJogadores++;
		return numeroJogadores;
	}
	
	private void setJogador(String chave, JogadorInstance jogador){
		jogadores.put(chave.toUpperCase(), jogador);
	}
	
	private JogadorInstance getJogador(String chave){
		return jogadores.get(chave.toUpperCase());
	}
}
