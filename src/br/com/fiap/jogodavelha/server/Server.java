package br.com.fiap.jogodavelha.server;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

import br.com.fiap.jogodavelha.definicoes.Acoes;

public class Server {
	public static void main(String[] args) throws IOException {

		ServerSocket serverSocket = null;
		JogadoresConectados jogadoresConectados = JogadoresConectados.getInstance();
		
		int porta = 4447;
		
		try {
			serverSocket = new ServerSocket(porta);
		} catch (IOException e) {
			System.err.println("Nao foi possivel ouvir a porta: " + porta);
			System.exit(1);
		}

		while(true) {
			Socket clientSocket = null;
			try {
				clientSocket = serverSocket.accept();
//				System.out.println("Recebendo o jogador");
				if (jogadoresConectados.temVaga()) {		
//					System.out.println("Dentro if");
					JogadorInstance ji = new JogadorInstance(clientSocket, jogadoresConectados);
					new Thread(ji).start();
//					System.out.println("Recebendo o jogador");
				} else {
					PrintWriter clientOut = new PrintWriter(clientSocket.getOutputStream(), true);
					clientOut.write(Acoes.DESCONECTA.getAcao() + "Tabuleiro est� cheio! Tente novamente mais tarde.");
					clientOut.flush();
					clientOut.close();
				}
				
			} catch (IOException e) {
				System.err.println("Falha ao aceitar o cliente");
				System.exit(1);
			}
		}
	}
}
