package br.com.fiap.jogodavelha.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;

import br.com.fiap.jogodavelha.definicoes.Acoes;
import br.com.fiap.jogodavelha.modelo.Jogada;
import br.com.fiap.jogodavelha.modelo.Jogador;
import br.com.fiap.jogodavelha.modelo.Tabuleiro;
import br.com.fiap.jogodavelha.modelo.TipoJogador;
import br.com.fiap.jogodavelha.suporte.XMLUtils;

public class JogadorInstance implements Runnable {

	private Socket client;
	private PrintWriter out;
	private BufferedReader in;
	private JogadoresConectados jogadores;
	private Jogador jogador;
	private static Tabuleiro tabuleiro = new Tabuleiro();

	public JogadorInstance(Socket client, JogadoresConectados jogadores) {
		this.client = client;
		this.jogadores = jogadores;

		try {
			out = new PrintWriter(this.client.getOutputStream(), true);
			in = new BufferedReader(new InputStreamReader(this.client.getInputStream()));			
		} catch (Exception e) {}
	}

	@Override
	public void run() {
		serve();
	}
	
	public void responde(String resposta) {
		out.println(resposta);
	}
	
	public void serve() {
		
		String inputLine;
		
		try {
			while ((inputLine = in.readLine()) != null) {
				processInput(inputLine);
			}
		} catch (SocketException e) {
			
		} catch (IOException e) {
			
		} finally {
		}
	}

	public void fechaConexao() {
		try {
			this.client.close();
		} catch (Exception e) {
		}
	}

	public void processInput(String theInput) {

		String[] comando = theInput.split(":", 2);
		
		switch(Acoes.valueOf(comando[0])) {
			case DESCONECTA:
				fechaConexao();
			break;
			
			case ENVIA_TABULEIRO:
				enviaTabuleiro();
			break;
			
			case ENVIA_JOGADA:
				recebeJogada(comando[1]);
				System.out.println(comando[1]);
			break;
			
			case REGISTRA_USUARIO:
				this.responde(Acoes.REGISTRA_USUARIO.getAcao() + svRegistraUsuario(comando[1]));
				if (this.jogadores.size() == 2){
					this.jogadores.getJogadorX().responde(Acoes.INICIIIA_PARTIDA.getAcao());
				}
			break;
			
			case NOVO_JOGO:
				tabuleiro = new Tabuleiro();
				tabuleiro.setJogadorUltimaJogada(jogadores.getJogadorO().jogador);
				enviaTabuleiro();
			break;
			default:
				System.out.println("A��o sem tratamento");
			break;
		}
		
	}
	
	private void recebeJogada(String comando){
		comando = comando.replaceAll("<enviajogada>", "");
		comando = comando.replace("</enviajogada>", "");
		
		Jogada jogada = XMLUtils.jogadaFromXml(comando);
		tabuleiro.marcaQuadrante(jogada.getQuadrante(), jogada.getJogador().getTipo().name());
		tabuleiro.setJogadorUltimaJogada(jogada.getJogador());
		
		enviaTabuleiro();
	}

	private void enviaTabuleiro() {
		StringBuilder tabuleiroASerEnviado = new StringBuilder();
		tabuleiroASerEnviado.append(Acoes.ENVIA_TABULEIRO.getAcao());
		tabuleiroASerEnviado.append(XMLUtils.tabuleiroToXml(tabuleiro));
		
		if(jogadores.size() == 2) {
			System.out.println(tabuleiroASerEnviado.toString());
			jogadores.getJogadorX().responde(tabuleiroASerEnviado.toString());
			jogadores.getJogadorO().responde(tabuleiroASerEnviado.toString());
		}
	}

	private String svRegistraUsuario(String comando) {
		this.jogador = XMLUtils.jogadorFromXml(comando);
		
		if (jogadores.getJogadorX() == null) {
			this.jogador.setTipo(TipoJogador.X);
			this.jogadores.setJogadorX(this);
		} else {
			this.jogador.setTipo(TipoJogador.O);
			this.jogadores.setJogadorO(this);
		}
		
		return jogador.getTipo().name();
	}
}
