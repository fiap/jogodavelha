package br.com.fiap.jogodavelha.suporte;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import br.com.fiap.jogodavelha.definicoes.Acoes;
import br.com.fiap.jogodavelha.modelo.Jogada;
import br.com.fiap.jogodavelha.modelo.Jogador;
import br.com.fiap.jogodavelha.modelo.TipoJogador;


public class Conexao {
	private String ipServidor;
	private Jogador jogador;
	private final int porta;
	private Socket conexao;
	private PrintWriter writer;
	private BufferedReader reader;

	public Conexao(String ipServidor, Jogador jogador) {
		this.ipServidor = ipServidor;
		this.jogador = jogador;
		this.porta = 4447;
	}
	
	public void open() throws ChatException {
		if(this.conexao == null){
			try{
				this.conexao = new Socket(this.ipServidor, this.porta);
				this.writer = new PrintWriter(this.conexao.getOutputStream(), true);
				this.reader = new BufferedReader(new InputStreamReader(this.conexao.getInputStream()));
			}catch (Exception e) {
				e.getStackTrace();
			}
			
			this.inscribeUser();
		}
	}
	
	public void open(String ipServidor, Jogador jogador) throws ChatException {
		this.ipServidor = ipServidor;
		this.jogador = jogador;
		
		this.open();
	}
	
	public void close(){
		try {
			this.sendCommand(Acoes.DESCONECTA, "");
			this.conexao.close();
		} catch (IOException e) {
			throw new RuntimeException("Erro ao fechar conexão: " + e.getMessage());
		}
	}
	
	public String receive() {
		try {
			return this.reader.readLine();
		} catch (IOException e) {
			throw new RuntimeException("Erro ao receber mensagem: " + e.getMessage());
		}
	}
	
	public void consultaStatusJogo(){
		this.sendCommand(Acoes.CONSULTA_TERMINO, "");
		String status = this.receive();
		
	}
	
	public void enviaJogada(Jogada jogada){
//		StringBuilder sb = new StringBuilder();
//		sb.append("<enviajogada>");
//		sb.append(XMLUtils.jogadaToXml(jogada));
//		sb.append(XMLUtils.jogadorToXml(jogador));
//		sb.append("</enviajogada>");
				
		this.sendCommand(Acoes.ENVIA_JOGADA, XMLUtils.jogadaToXml(jogada));
	}
	
	public void solicitaNovoJogo(){
		this.sendCommand(Acoes.NOVO_JOGO, "");
	}
	
	public void getRegisteredUsers(){
		this.sendCommand(Acoes.LISTA_USUARIO, "");
	}
	
	private void inscribeUser() throws ChatException{

		this.sendCommand(Acoes.REGISTRA_USUARIO, XMLUtils.jogadorToXml(this.jogador));
		
		String[] partesRetorno = this.receive().split(":", 2);
		
		if (Acoes.valueOf(partesRetorno[0]) != Acoes.REGISTRA_USUARIO) {			
			this.close();
			throw new ChatException("Erro ao registrar usuario");
		}
		
		this.jogador.setTipo(TipoJogador.valueOf(partesRetorno[1]));
	}
	
	private void sendCommand(Acoes acao, String msg){
		StringBuilder mensagem = new StringBuilder(acao.toString());
		mensagem.append(msg);
		mensagem.append("\n");
		
		this.writer.write(mensagem.toString());
		this.writer.flush();
	}

	public boolean isRegistered() {
		return !this.conexao.isClosed();
	}
}
