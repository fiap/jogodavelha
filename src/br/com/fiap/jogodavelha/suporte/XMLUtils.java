package br.com.fiap.jogodavelha.suporte;

import br.com.fiap.jogodavelha.modelo.Jogada;
import br.com.fiap.jogodavelha.modelo.Jogador;
import br.com.fiap.jogodavelha.modelo.Quadrante;
import br.com.fiap.jogodavelha.modelo.Tabuleiro;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

public class XMLUtils {
	
	public static String jogadaToXml(Jogada jogada){
		XStream xStream = new XStream(new DomDriver());
		xStream.alias("jogada", Jogada.class);
		xStream.alias("jogador", Jogador.class);
		
		
		return xStream.toXML(jogada).replaceAll("\\n", "");
	}
	
	public static Jogada jogadaFromXml(String jogada){
		XStream xStream = new XStream(new DomDriver());
		xStream.alias("jogada", Jogada.class);
		xStream.alias("jogador", Jogador.class);
		
		return (Jogada)xStream.fromXML(jogada);
	}
	
	public static String jogadorToXml(Jogador jogador){
		XStream xStream = new XStream(new DomDriver());
		xStream.alias("jogador", Jogador.class);
		
		return xStream.toXML(jogador).replaceAll("\\n", "");
	}
	
	public static Jogador jogadorFromXml(String jogador){
		XStream xStream = new XStream(new DomDriver());
		xStream.alias("jogador", Jogador.class);
		
		return (Jogador)xStream.fromXML(jogador);
	}
	
	public static String tabuleiroToXml(Tabuleiro tabuleiro){
		XStream xStream = new XStream(new DomDriver());
		xStream.alias("tabuleiro", Tabuleiro.class);
		xStream.alias("quadrante", Quadrante.class);
		
		
		return xStream.toXML(tabuleiro).replaceAll("\\n", "");
	}
	
	public static Tabuleiro tabuleiroFromXml(String tabuleiro){
		XStream xStream = new XStream(new DomDriver());
		xStream.alias("tabuleiro", Tabuleiro.class);
		xStream.alias("quadrante", Quadrante.class);
		xStream.alias("jogadorUltimaJogada", Jogador.class);
		
		return (Tabuleiro)xStream.fromXML(tabuleiro);
	}
}
