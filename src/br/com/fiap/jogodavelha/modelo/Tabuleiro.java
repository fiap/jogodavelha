package br.com.fiap.jogodavelha.modelo;

import java.util.ArrayList;
import java.util.List;


public class Tabuleiro {
	private Quadrante[] quadrantes = new Quadrante[9];
	private Jogador jogadorUltimaJogada;
	
	public Tabuleiro() {
		for (int i = 0; i < quadrantes.length;) {
			quadrantes[i] = new Quadrante(++i, "-");
		}
	}
		
	public void marcaQuadrante(int codigoQuadrante, String valor){
		if ("-".equals(quadrantes[codigoQuadrante].getValor())) {			
			quadrantes[codigoQuadrante].setValor(valor);
		}
	}
	
	public Quadrante[] getQuadrantes(){
		return this.quadrantes;
	}
	
	public Integer[] getJogadasDisponiveis(){
		List<Integer> jogadasDisponiveis = new ArrayList<Integer>();
		for (int i = 0; i < quadrantes.length; i++) {
			if ("-".equals(quadrantes[i].getValor())) {				
				jogadasDisponiveis.add(quadrantes[i].getCodigo());
			}
		}
		
		return (Integer[])jogadasDisponiveis.toArray();
	}
	
	public boolean temVencedor(){
		if (verificaHorizontal() ||
			verificaVertical() ||
			verificaDiagonal1() ||
			verificaDiagonal2())
			return true;
		
		return false;
	}
	
	private boolean verificaHorizontal(){
		int j = 0;
		for (int i = 0; i < 3; i++) {			
			if (
				(quadrantes[j].isBolinha() &&
				quadrantes[j+1].isBolinha() &&
				quadrantes[j+2].isBolinha()) ||
				(quadrantes[j].isXizinho() &&
				quadrantes[j+1].isXizinho() &&
				quadrantes[j+2].isXizinho())
			) {
				return true;
			} else {
				j += 3;
			}			
		}
		
		return false;
	}
	
	private boolean verificaVertical(){
		int j = 0;
		for (int i = 0; i < 3; i++) {			
			if (
				(quadrantes[j].isBolinha() &&
				quadrantes[j+3].isBolinha() &&
				quadrantes[j+6].isBolinha()) ||
				(quadrantes[j].isXizinho() &&
				quadrantes[j+3].isXizinho() &&
				quadrantes[j+6].isXizinho())
			) {
				return true;
			} else {
				j += 1;
			}			
		}
		
		return false;
	}
	
	private boolean verificaDiagonal1(){
		if (
			(quadrantes[0].isBolinha() &&
			quadrantes[4].isBolinha() &&
			quadrantes[8].isBolinha()) ||
			(quadrantes[0].isXizinho() &&
			quadrantes[4].isXizinho() &&
			quadrantes[8].isXizinho())
		) {
			return true;
		} 
		
		return false;
	}
	
	private boolean verificaDiagonal2(){
		if (
			(quadrantes[2].isBolinha() &&
			quadrantes[4].isBolinha() &&
			quadrantes[6].isBolinha()) ||
			(quadrantes[2].isXizinho() &&
			quadrantes[4].isXizinho() &&
			quadrantes[6].isXizinho())
		) {
			return true;
		}
		
		return false;
	}
	
	public boolean hasNextMove(){
		if (this.temVencedor())
			return false;
		
		for (int i = 0; i < quadrantes.length; i++) {
			if (quadrantes[i].isEmpty()) {
				return true;
			}
		}
		
		return false;
	}
	
	public boolean isNextPlayer(Jogador jogador){
		return !jogadorUltimaJogada.equals(jogador);
	}

	public Jogador getJogadorUltimaJogada() {
		return jogadorUltimaJogada;
	}

	public void setJogadorUltimaJogada(Jogador jogadorUltimaJogada) {
		this.jogadorUltimaJogada = jogadorUltimaJogada;
	}
}
