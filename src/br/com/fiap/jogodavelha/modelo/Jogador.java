package br.com.fiap.jogodavelha.modelo;

public class Jogador {
	private final String nome;
	private TipoJogador tipo;

	public Jogador(String nome) {
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public TipoJogador getTipo() {
		return tipo;
	}

	public void setTipo(TipoJogador tipo) {
		this.tipo = tipo;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		
		if (obj.getClass() != this.getClass()) {
			return false;
		}
		
		Jogador aComparar = (Jogador) obj;
		
		return this.tipo == aComparar.tipo;
	}
	
}