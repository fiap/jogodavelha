package br.com.fiap.jogodavelha.modelo;

public class Quadrante {
	private int codigo;
	private String valor;
	
	public Quadrante(int codigo, String valor) {
		this.codigo = codigo;
		this.valor = valor;
	}
	
	public int getCodigo() {
		return codigo;
	}
	
	public String getValor() {
		return valor;
	}
	
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	
	public void setValor(String valor) {
		this.valor = valor;
	}
	
	public boolean isEmpty() {
		return "-".equalsIgnoreCase(this.valor);
	}
	
	public boolean isBolinha(){
		return "O".equalsIgnoreCase(this.valor);
	}
	
	public boolean isXizinho(){
		return "X".equalsIgnoreCase(this.valor);
	}
}
