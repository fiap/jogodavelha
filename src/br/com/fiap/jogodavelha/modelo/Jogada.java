package br.com.fiap.jogodavelha.modelo;


public class Jogada {
	private final int quadrante;
	private final Jogador jogador;

	public Jogada(int quadrante, Jogador jogador) {
		this.quadrante = quadrante;
		this.jogador = jogador;
	}

	public int getQuadrante() {
		return quadrante;
	}

	public Jogador getJogador() {
		return jogador;
	}
}
