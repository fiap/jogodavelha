package br.com.fiap.jogodavelha.definicoes;

public enum Acoes {
	ENVIA_JOGADA, CONSULTA_TERMINO, ENVIA_TABULEIRO, REGISTRA_USUARIO, ENVIA_MENSAGEM, DESCONECTA, LISTA_USUARIO, INICIIIA_PARTIDA, NOVO_JOGO;
	
	public String toString() {
		return this.name() + ":";
	}
	
	public String getAcao() {
		return this.name() + ":";
	}
}
